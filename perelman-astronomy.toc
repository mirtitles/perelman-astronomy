\contentsline {chapter}{Foreword}{v}{chapter*.1}%
\contentsline {chapter}{Preface}{vii}{chapter*.2}%
\contentsline {chapter}{\numberline {1}The Earth, Its Shape And Motions}{1}{chapter.1}%
\contentsline {section}{\numberline {1}The Shortest Way: on Earth and Map}{2}{section.1.1}%
\contentsline {section}{\numberline {2}Degree of Longitude and Degree of Latitude Question}{11}{section.1.2}%
\contentsline {section}{\numberline {3}In What Direction Did Amundsen Fly?}{12}{section.1.3}%
\contentsline {section}{\numberline {4}Five Ways of Reckoning Time}{13}{section.1.4}%
\contentsline {section}{\numberline {5}Duration of Daylight}{19}{section.1.5}%
\contentsline {section}{\numberline {6}Extraordinary Shadows}{21}{section.1.6}%
\contentsline {section}{\numberline {7}The Problem of the Two Trains}{24}{section.1.7}%
\contentsline {section}{\numberline {8}The Pocket-Watch as Compass}{26}{section.1.8}%
\contentsline {section}{\numberline {9}``White'' Nights and ``Black'' Days}{29}{section.1.9}%
\contentsline {section}{\numberline {10}Daylight and Darkness}{31}{section.1.10}%
\contentsline {section}{\numberline {11}The Riddle of the Polar Sun }{33}{section.1.11}%
\contentsline {section}{\numberline {12}When Do the Seasons Begin?}{34}{section.1.12}%
\contentsline {section}{\numberline {13}Three ``Ifs''}{38}{section.1.13}%
\contentsline {section}{\numberline {14}One More “If”}{43}{section.1.14}%
\contentsline {section}{\numberline {15}If the Earth's Path Were More Extended}{47}{section.1.15}%
\contentsline {section}{\numberline {16}When Are We Nearer to the Sun, Noon or Evening?}{51}{section.1.16}%
\contentsline {section}{\numberline {17}Add a Metre}{52}{section.1.17}%
\contentsline {section}{\numberline {18}From Different Points of View}{54}{section.1.18}%
\contentsline {section}{\numberline {19}Unearthly Time}{58}{section.1.19}%
\contentsline {section}{\numberline {20}Where Do the Months and Years Begin?}{61}{section.1.20}%
\contentsline {section}{\numberline {21}How Many Fridays Are There in February?}{64}{section.1.21}%
\contentsline {chapter}{\numberline {2}The Moon And Its Motions}{67}{chapter.2}%
\contentsline {section}{\numberline {22}New Moon or Old?}{68}{section.2.22}%
\contentsline {section}{\numberline {23}The Moon on Flags}{69}{section.2.23}%
\contentsline {section}{\numberline {24}The Riddle of the Lunar Phases}{70}{section.2.24}%
\contentsline {section}{\numberline {25}The Double Planet}{72}{section.2.25}%
\contentsline {section}{\numberline {26}Why Doesn't the Moon Fall onto the Sun?}{75}{section.2.26}%
\contentsline {section}{\numberline {27}The Moon's Visible and Invisible Faces}{77}{section.2.27}%
\contentsline {section}{\numberline {28}A Second Moon and the Moon's Moon}{83}{section.2.28}%
\contentsline {section}{\numberline {29}Why Is There No Air on the Moon?}{85}{section.2.29}%
\contentsline {section}{\numberline {30}Lunar Dimensions}{88}{section.2.30}%
\contentsline {section}{\numberline {31}Lunar Landscapes}{92}{section.2.31}%
\contentsline {section}{\numberline {32}Lunar Heavens}{99}{section.2.32}%
\contentsline {section}{\numberline {33}Lunar Eclipses}{106}{section.2.33}%
\contentsline {section}{\numberline {34}Why Do Astronomers Observe Eclipses?}{107}{section.2.34}%
\contentsline {section}{\numberline {35}Why Do Eclipses Recur Every Eighteen Years}{117}{section.2.35}%
\contentsline {section}{\numberline {36} Can It Happen?}{121}{section.2.36}%
\contentsline {section}{\numberline {37} What Not All Know About Eclipses}{124}{section.2.37}%
\contentsline {section}{\numberline {38}What Is Lunar Weather Like?}{127}{section.2.38}%
\contentsline {chapter}{\numberline {3}The Planets}{131}{chapter.3}%
\contentsline {section}{\numberline {39}The Planets in Daylight}{131}{section.3.39}%
\contentsline {section}{\numberline {40}The Planetary Alphabet}{133}{section.3.40}%
\contentsline {section}{\numberline {41}Something We Cannot Draw}{135}{section.3.41}%
\contentsline {section}{\numberline {42}Why Is There No Atmosphere on Mercury?}{139}{section.3.42}%
\contentsline {section}{\numberline {43}The Phases of Venus}{141}{section.3.43}%
\contentsline {section}{\numberline {44}The Most Favourable Opposition}{143}{section.3.44}%
\contentsline {section}{\numberline {45}A Planet or Minor Sun?}{146}{section.3.45}%
\contentsline {section}{\numberline {46}Saturn’s Rings Disappear}{150}{section.3.46}%
\contentsline {section}{\numberline {47}Astronomical Anagrams}{152}{section.3.47}%
\contentsline {section}{\numberline {48}Trans-Neptunian Planet}{154}{section.3.48}%
\contentsline {section}{\numberline {49}Pigmy Planets}{159}{section.3.49}%
\contentsline {section}{\numberline {50}Our Nearest Neighbours}{162}{section.3.50}%
\contentsline {section}{\numberline {51}Jupiter’s Fellow-Travellers}{164}{section.3.51}%
\contentsline {section}{\numberline {52}Alien Skies}{165}{section.3.52}%
\contentsline {chapter}{\numberline {4}The Stars}{179}{chapter.4}%
\contentsline {section}{\numberline {53}Why Do Stars Look Like Stars?}{179}{section.4.53}%
\contentsline {section}{\numberline {54}Why Do Stars Twinkle While Planets Shine Steadily?}{180}{section.4.54}%
\contentsline {section}{\numberline {55}Can Stars Be Seen in Daylight?}{183}{section.4.55}%
\contentsline {section}{\numberline {56}What Is Stellar Magnitude?}{185}{section.4.56}%
\contentsline {section}{\numberline {57}Stellar Algebra}{187}{section.4.57}%
\contentsline {section}{\numberline {58}The Eye and the Telescope}{191}{section.4.58}%
\contentsline {section}{\numberline {59}Stellar Magnitude of Sun and Moon}{192}{section.4.59}%
\contentsline {section}{\numberline {60}True Brilliance of Stars and Sun}{195}{section.4.60}%
\contentsline {section}{\numberline {61}Brightest of Known Stars}{197}{section.4.61}%
\contentsline {section}{\numberline {62}Stellar Magnitude of Planets as Seen in Our Sky and in Alien Skies}{198}{section.4.62}%
\contentsline {section}{\numberline {63}Why Are the Stars Not Magnified in the Telescope?}{200}{section.4.63}%
\contentsline {section}{\numberline {64}How Were Stellar Diameters Measured?}{204}{section.4.64}%
\contentsline {section}{\numberline {65}Giants of the Stellar World}{206}{section.4.65}%
\contentsline {section}{\numberline {66}An Unexpected Result}{207}{section.4.66}%
\contentsline {section}{\numberline {67}The Heaviest Substance}{208}{section.4.67}%
\contentsline {section}{\numberline {68}Why Are Stars Called Fixed Stars?}{213}{section.4.68}%
\contentsline {section}{\numberline {69}Units of Stellar Distances}{215}{section.4.69}%
\contentsline {section}{\numberline {70}The Nearest Stellar Systems}{219}{section.4.70}%
\contentsline {section}{\numberline {71}The Scale of the Universe}{221}{section.4.71}%
\contentsline {chapter}{\numberline {5}Gravitation}{225}{chapter.5}%
\contentsline {section}{\numberline {72}Shooting Vertically}{225}{section.5.72}%
\contentsline {section}{\numberline {73}Weight at High Altitudes}{229}{section.5.73}%
\contentsline {section}{\numberline {74}With Compasses Along Planetary Paths}{232}{section.5.74}%
\contentsline {section}{\numberline {75}When Planets Fall onto the Sun}{237}{section.5.75}%
\contentsline {section}{\numberline {76}Vulcan’s Forge}{241}{section.5.76}%
\contentsline {section}{\numberline {77}The Boundaries of the Solar System}{242}{section.5.77}%
\contentsline {section}{\numberline {78}The Error in Jules Verne’s Book}{243}{section.5.78}%
\contentsline {section}{\numberline {79}How Was the Earth Weighed?}{244}{section.5.79}%
\contentsline {section}{\numberline {80}What Is Inside the Earth?}{247}{section.5.80}%
\contentsline {section}{\numberline {81}Weighing the Sun and the Moon}{248}{section.5.81}%
\contentsline {section}{\numberline {82}Weight and Density of Planets and Stars}{252}{section.5.82}%
\contentsline {section}{\numberline {83}Weight on the Moon and on the Planets}{253}{section.5.83}%
\contentsline {section}{\numberline {84}Record Weight}{256}{section.5.84}%
\contentsline {section}{\numberline {85} Weight in the Depths of the Planet}{257}{section.5.85}%
\contentsline {section}{\numberline {86}The Problem of the Steamer}{258}{section.5.86}%
\contentsline {section}{\numberline {87}Lunar and Solar Tides}{261}{section.5.87}%
\contentsline {section}{\numberline {88}The Moon and Weather}{264}{section.5.88}%
